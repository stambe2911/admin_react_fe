
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import store from './Store';

const ProtectedRoute = ({ component: Component, ...rest }) => { // Passing (i) "component" as "props" and calling it as "Component" and (ii) rest of the remaining properties that are sent. (used curley braces for destructuring)

    const reduxState = store.getState();
    const { adminId } = reduxState.adminR;

    return (
        <Route {...rest} render={props => {
            //return <Component {...props} />;  // To prevent Login again and again while coding

            if (adminId) {
                return <Component {...props} />;

            } else {
                return (
                    <Redirect to={{
                        pathname: '/',
                        state: {
                            from: props.location
                        }
                    }} />
                );
            }
        }} />
    );
}

export default ProtectedRoute;