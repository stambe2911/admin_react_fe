import React, { Component } from "react";

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import ProtectedRoute from './ProtectedRoute';

import Login from "./Component/pages/Login";
import Dashboard from "./Component/pages/Dashboard";

import { Provider } from 'react-redux';
import store from './Store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path='/' component={Login} />
            <ProtectedRoute exact path='/dashboard' component={Dashboard} />
            <Route component={() => { return <h2>404 Page Not Found</h2>; }} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
