
import { SET_ADMIN, REMOVE_ADMIN } from '../actions/types';

const initialState = {
    adminId: '',
    username: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_ADMIN:
            return {
                ...state,
                adminId: action.adminId,
                username: action.username
            };

        case REMOVE_ADMIN:
            return {
                ...state,
                adminId: '',
                username: ''
            };

        default:
            return {
                ...state
            };
    }
}