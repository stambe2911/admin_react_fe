import { combineReducers } from 'redux';
import layoutReducer from './layoutReducer';
import adminReducer from './adminReducer';

export default combineReducers(
    {
        layoutR: layoutReducer,
        adminR: adminReducer
    }
);