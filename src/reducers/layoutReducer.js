
import { CHANGE_SIDE_DRAWER_STATUS, SWITCH_PAGE, TOGGLE_PAGE_LOADER_STATUS } from '../actions/types';

const initialState = {
    sideDrawerStatus: "active",
    activePageLink: "statisticsLink",
    activePageName: "statisticsPage",
    pageLoaderStatus: "activeThing"
};

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGE_SIDE_DRAWER_STATUS:
            var sideDrawerNextStatus = "inactive";
            if (state.sideDrawerStatus === "inactive") {
                sideDrawerNextStatus = "active";
            }

            return {
                ...state,
                sideDrawerStatus: sideDrawerNextStatus
            };

        case SWITCH_PAGE:
            return {
                ...state,
                activePageLink: action.pageLink,
                activePageName: action.pageName
            };

        case TOGGLE_PAGE_LOADER_STATUS:
            var pageLoaderNextStatus = "inactiveThing";
            if (state.pageLoaderStatus === "inactiveThing") {
                pageLoaderNextStatus = "activeThing";
            }

            return {
                ...state,
                pageLoaderStatus: pageLoaderNextStatus
            };

        default:
            return state;
    }
}