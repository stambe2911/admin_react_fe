import React, { Component } from 'react';

import { TextInput } from '../Layout/InputGroups';

import axios from 'axios';
import classnames from 'classnames';

import uploadingLoaderImg from '../../custom/images/uploading.gif';
import loadingMoreLoaderImg from '../../custom/images/loadingMore.gif';

import DateTimePicker from 'react-datetime-picker'; // Date and Time select library (It doesn't require 'moment.js')
import moment from 'moment';  // Datetime library (To change 'Date' object's format)

class Events extends Component {

    state = {
        title: '',
        time: new Date(),
        error: {},
        uploadMessage: {},
        uploadingEvent: false,
        allEventsPageNo: 0,
        allEvents: [],
        loadingMore: false,
        showLoadingMoreImg: false
    };

    async componentDidMount() {
        this.loadEvents();  // >this.functionName< (ie. No ()) only works inside 'render()'.
    }

    onInputChange = (e) => {
        const { name, value } = e.target;

        this.setState({
            [name]: value
        });
    }

    onDateTimeChange = (value) => {
        this.setState({
            time: value
        });
    }

    uploadEvent = async (e) => {
        e.preventDefault();

        this.setState({
            error: {},
            uploadMessage: {}
        });

        let { title, time } = this.state;

        title = title.trim();
        time = moment(time).format('YYYY-MM-DD HH:mm:ss');

        if (title === '') {
            this.setState({
                error: { 'title': 'Title is Mandatory' }
            });

            return;
        }

        this.setState({
            uploadingEvent: true,
            allEvents: [],
            loadingMore: true,
            showLoadingMoreImg: true
        });

        const formData = {  // Another way than >new FormData()<
            eventTitle: title,
            eventTime: time
        };

        let response = await axios.post('http://localhost:8000/api/uploadEvent', formData, {});
        response = response.data;

        if (response.status === 'success') {
            this.setState({
                uploadMessage: {
                    status: 1,
                    message: response.message
                },
                title: '',
                time: new Date(),
                uploadingEvent: false,
                allEventsPageNo: 0
            });

            this.loadEvents();

        } else {
            this.setState({
                uploadMessage: {
                    status: 0,
                    message: response.message
                },
                uploadingEvent: false
            });
        }
    }

    loadEvents = async () => {
        this.setState({
            loadingMore: true,
            showLoadingMoreImg: true
        });

        let { allEventsPageNo } = this.state;
        allEventsPageNo = ++allEventsPageNo;

        let allEvents = await axios.post('http://localhost:8000/api/getEvents', { 'pageNo': allEventsPageNo }, {});
        allEvents = allEvents.data;

        if (allEvents.length > 0) {
            this.setState({
                loadingMore: false,
                showLoadingMoreImg: false,
                allEventsPageNo,
                allEvents: [...this.state.allEvents, ...allEvents]
            });

        } else {
            this.setState({
                loadingMore: true,  // Disable >MORE< permanently
                showLoadingMoreImg: false,
                allEventsPageNo,
                allEvents: [...this.state.allEvents, ...allEvents]
            });
        }
    }

    render() {
        const { title, time, error, uploadingEvent, uploadMessage, allEvents, loadingMore, showLoadingMoreImg } = this.state;

        return (
            <div>
                <h2>EVENTS</h2>

                <div className='formDiv'>
                    <TextInput
                        label='Title* :'
                        name='title'
                        htmlFor='eventTitle'
                        value={title}
                        placeholder="Event Title"
                        onChange={this.onInputChange}
                        error={error.title}
                    />

                    <div className='form-group'>
                        <div className='row'>
                            <label className='col-md-2'>Time* :</label>
                            <DateTimePicker
                                format="dd-MM-y h:mm:ss a"
                                minDate={new Date()}
                                calendarIcon={null}
                                clearIcon={null}
                                name='time'
                                onChange={this.onDateTimeChange}
                                value={time}
                            />
                        </div>
                    </div>

                    <div className={classnames('row col-md-12', {
                        'messageSuccess': uploadMessage.status === 1,
                        'messageFail': uploadMessage.status === 0
                    })}>
                        {uploadMessage.message}
                    </div>
                    <div className='row'>
                        {uploadingEvent && <img src={uploadingLoaderImg} alt='loader' className='uploadingLoader' />}
                    </div>

                    <div className='row'>
                        <button type='button' className='btn btn-primary' onClick={this.uploadEvent} disabled={uploadingEvent}>UPLOAD EVENT</button>
                    </div>
                </div>

                <hr />

                {
                    allEvents.map((event, index) => {
                        return (
                            <div className='row' key={event.eventId} style={{ 'marginTop': '15px' }}>
                                <div className='card border-primary col-md-10'>
                                    <div className='card-body'>
                                        {event.eventId} {' > '} {event.eventTitle}
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }

                <div className='row'>
                    {showLoadingMoreImg && <img src={loadingMoreLoaderImg} alt='Loading More...' className='offset-md-4 loadingMoreLoaderImg' />}
                </div>

                <div className='row' style={{ 'marginTop': '20px' }}>
                    <button
                        type='button'
                        className='btn btn-info offset-md-5'
                        onClick={this.loadEvents}
                        disabled={loadingMore}
                    >
                        MORE...
                    </button>
                </div>

                <div className="spaceDiv-md"></div>

            </div>
        );
    }
}

export default Events;