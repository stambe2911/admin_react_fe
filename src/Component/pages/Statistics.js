
import React, { Component } from 'react';
import axios from 'axios';

import { connect } from 'react-redux';
import { togglePageLoaderStatus } from '../../actions/layoutActions';

class Statistics extends Component {

    state = {
        counts: {
            'activePosts': 0,
            'activeEvents': 0,
            'activeUsers': 0
        }
    };

    async componentDidMount() {
        let response = await axios.get('http://localhost:8000/getStatistics');

        this.setState({
            counts: response.data
        });

        setTimeout(() => {
            this.props.togglePageLoaderStatus();
        }, 3000);
    }

    render() {
        const { activePosts, activeEvents, activeUsers } = this.state.counts;

        return (
            <div>
                <h1>Statistics</h1>

                <div className="row">
                    <div className="card statistic-card col-md-2">
                        <div className="card-body">
                            Posts <br /> {activePosts}
                        </div>
                    </div>
                    <div className="card statistic-card col-md-2">
                        <div className="card-body">
                            Events <br /> {activeEvents}
                        </div>
                    </div>
                    <div className="card statistic-card col-md-2">
                        <div className="card-body">
                            Users <br /> {activeUsers}
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default connect(null, { togglePageLoaderStatus })(Statistics);