
import React, { Component } from 'react';
import { connect } from 'react-redux';

import "../../css/dashboard.css";
import '../../css/sideDrawer.css';

import SideBar from "../Layout/SideBar";
import PageContent from "../Layout/PageContent";

import pageLoader from '../../custom/images/pageLoader.gif';

const pageLoaderDivStyle = {
    position: 'fixed',
    zIndex: '1200',
    width: '100%',
    height: '100%',
    textAlign: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)'
};

const pageLoaderImgStyle = {
    position: 'relative',
    width: '250px',
    verticalAlign: 'middle',
    margin: '0 auto',
    textAlign: 'center',
    top: '45%'
};

class Dashboard extends Component {

    render() {
        const { pageLoaderStatus } = this.props.layoutStatus;

        return (
            <div className="App wrapper">
                <div style={pageLoaderDivStyle} className={pageLoaderStatus}><img src={pageLoader} alt="loader" style={pageLoaderImgStyle} /></div>
                <SideBar />
                <PageContent />
            </div>
        );
    }
}

const mapsStateToProps = (state) => ({
    layoutStatus: state.layoutR
});

export default connect(mapsStateToProps, null)(Dashboard);