
import React, { useState } from 'react';

import login_styles from '../../css/loginPage.module.css';
import logo from '../../custom/images/AB-logo.png';
import loading from '../../custom/images/uploading.gif';

import { connect } from 'react-redux';
import { set_admin } from '../../actions/adminActions';

import { useHistory } from 'react-router';  // Because, we can't use >this.props.history.push()< in functional component

import axios from 'axios';

// 'useState' hook example
function Login({ set_admin }) { // Passing Redux function as props

    let [username, setUsername] = useState('');
    // The first parameter, 'username' is what we are going to name our 'key in state'.
    // The second parameter, 'setUsername' is what we are going to use to 'set the username'.
    // >useState('')< will set default value to ''.
    // Did'n use >const<. Becoz, we have to do >trim()< for validation.

    let [password, setPassword] = useState('');
    const [loginMessage, setLoginMessage] = useState('');
    const [verifying, setVerifying] = useState(false);

    /* Comparison of above to class component */
    /*
    state = {
        username: '',
        password: '',
        loginMessage: ''
    }
    */

    const history = useHistory();  // This need to be inside functional component BUT, out of any arrow function.

    const onChange = (e) => {
        const { name, value } = e.target;

        if (name === 'username') {
            setUsername(value);  // Calling >useState()< method

        } else if (name === 'password') {
            setPassword(value);
        }

    }

    // Because it is a functional component you have to make function 'const'.
    const login = async (e) => {
        e.preventDefault();

        setVerifying(true);
        setLoginMessage('');

        // We can use 'state' values directly, without the need of >this.state.keyName<
        username = username.trim();
        password = password.trim();

        if (username === '' || password === '') {
            setLoginMessage('Both fields are mandatory');
            return;
        }

        const admin = {
            username,
            password
        };

        let response = await axios.post('http://localhost:8000/verifyAdmin', admin);

        setVerifying(false);

        if (response.data.length > 0) {
            const { adminId, username } = response.data[0];

            set_admin(adminId, username);

            history.push('/dashboard');

        } else {
            setLoginMessage('Incorrect Credentials');
        }
    }

    const handleKeyPressOnLoginDiv = (e) => {
        if (e.keyCode === 13) {
            login(e);   // No >this.login()<. Becoz, its a functional component
        }
    }

    // No need of >render()< in functional component
    return (
        <div className={login_styles.loginPage} onKeyDown={handleKeyPressOnLoginDiv}>
            <div className='row'>
                <div className={`${login_styles.loginDiv} ${login_styles.bounceIn} col-sm-8 col-md-3 col-lg-3`}>
                    <div style={{ textAlign: 'center' }}>
                        <h2>Welcome Admin</h2>
                    </div>
                    <div style={{ textAlign: 'center' }}>
                        <img src={logo} alt='logo' style={{ height: 'auto', width: '150px' }} />
                    </div>
                    <div className='form-group'>
                        <input type='text' name='username' className={`${login_styles.form_control} input-lg form-control`} placeholder='Username' value={username} onChange={onChange} />
                    </div>
                    <div className='form-group'>
                        <input type='password' name='password' className={`${login_styles.form_control} input-lg form-control`} placeholder='Password' value={password} onChange={onChange} />
                    </div>
                    <div style={{ textAlign: 'center' }}>
                        <h5 style={{ color: 'red' }}>{loginMessage}</h5>
                    </div>
                    <div
                        className={`${login_styles.verifyingLogoDiv}`}
                        style={
                            verifying ? { 'display': 'block' } : { 'display': 'none' }
                        }
                    >
                        Verifying... <img src={loading} alt='loading' />
                    </div>
                    <div className='form-group'>
                        <input type='button' className={`${login_styles.login_button} btn btn-block btn-lg`} value='LOGIN' onClick={login} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default connect(null, { set_admin })(Login);