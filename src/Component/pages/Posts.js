
import React, { Component, useState } from 'react';  // useState for reactstrap 

import { connect } from 'react-redux';
import { togglePageLoaderStatus } from '../../actions/layoutActions';

import axios from 'axios';
import classnames from 'classnames';
import Compressor from 'compressorjs'; // $ npm install compressorjs // https://github.com/fengyuanchen/compressorjs

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import uploading from '../../custom/images/uploading.gif';

import { TextInput, TextAreaInput, MultipleFilesInput, RadioGroupInput, CheckBoxInput, SelectDdInput, CheckboxDdInput } from '../Layout/InputGroups';

import { Table } from '../Layout/Table';  // react-table

class Posts extends Component {
    state = {
        uploadingPost: false,
        title: '',
        description: '',
        categories: [
            { label: 'Select Category', value: '0' }
        ],
        category: '0',   // ie. { label: 'Category', value: '0' }  // Current Chosen Category
        departments: [
            { name: 'all', label: 'All', htmlFor: 'allPostDepartments', isChecked: false }
        ],
        cities: [
            { name: 'all', label: 'All', htmlFor: 'allPostCities', isChecked: false }
        ],
        images: '',
        shareable: '1',
        inquiryPost: false,
        customerSpeakPost: true,
        error: {},
        uploadMessage: {},
        allPosts: [],
        allPostsColumns: [],
        currentSearchInReactTable: ''  // Other than blank string, not working (BUG).
    }

    async componentDidMount() {
        this.updateAllPosts();

        // We could have put this in default 'state', but we need to use 'addOrRemovePost()' too.
        const allPostsColumns = [
            {
                accessor: 'postId',
                Header: 'Id'
            },
            {
                accessor: 'title',
                Header: 'Title'
            },
            {
                accessor: 'status',
                Header: 'Status',
                Cell: cellInfo => {
                    const status = cellInfo.value;
                    return (
                        status == 1 ? (<span className='messageSuccess'>Added</span>) : (<span className='messageFail'>Removed</span>)
                    );
                }
            },
            {
                accessor: 'btnStatus',
                Header: '',
                disableSortBy: true,
                Cell: cellInfo => (
                    // 'cellInfo.value' contains 'accessor' value.
                    // 'cellInfo.row.original' contains whole row info as object.  

                    <React.Fragment>
                        <AddOrRemovePostBtn postId={cellInfo.row.original.postId} addOrRemove='1' handleOnClick={this.addOrRemovePost} disabled={cellInfo.value[0]} />
                        <br /><br />
                        <AddOrRemovePostBtn postId={cellInfo.row.original.postId} addOrRemove='0' handleOnClick={this.addOrRemovePost} disabled={cellInfo.value[1]} />
                    </React.Fragment>
                )
            },
            {
                accessor: 'editBtn',
                Header: '',
                disableSortBy: true,
                Cell: cellInfo => (
                    <PostEdit
                        btnColor='primary'
                        btnClass=''
                        disabled={false}
                        postId={cellInfo.value}
                        updateAllPosts={this.updateAllPosts}
                    />
                )
            }
        ];


        let allCategories = await axios.get('http://localhost:8000/getCategories');

        allCategories = allCategories.data.map((category, index) => {
            return { label: category.category, value: category.categoryId }
        });


        let allDepartments = await axios.get('http://localhost:8000/getDepartments');

        allDepartments = allDepartments.data.map((department, index) => {
            return { name: department.departmentId, label: department.department, htmlFor: 'postDepartment' + department.departmentId, isChecked: false }
        });


        let allCities = await axios.get('http://localhost:8000/getCities');

        allCities = allCities.data.map((city, index) => {
            return { name: city.cityId, label: city.city, htmlFor: 'postCity' + city.cityId, isChecked: false }
        });


        this.setState({
            allPostsColumns,
            categories: this.state.categories.concat(allCategories),
            departments: this.state.departments.concat(allDepartments),
            cities: this.state.cities.concat(allCities)
        });
    }

    updateAllPosts = async () => {
        let allPosts = await axios.get('http://localhost:8000/posts');
        allPosts = allPosts.data;

        // Not Necessary. But, 'react-table' needs unique 'accessor' for each column.
        allPosts.forEach(function (value, key) {

            value.btnStatus = value.status === 1 ? [true, false] : [false, true];  // 'status' for 'disabled' property of Buttons. [forAdd, forRemove]

            value.editBtn = value.postId;
        });

        this.setState({
            allPosts
        });
    }

    // input[type=text, radio, select DD], textarea
    onInputChange = (e) => {
        const { name, value } = e.target;

        this.setState({
            [name]: value
        });
    }

    onFileInputChange = (e) => {
        const { name } = e.target;

        this.setState({
            [name]: e.target.files
        });
    }

    onCheckBoxInputChange = (e) => {
        const { name } = e.target;

        let currentState = this.state[name];

        this.setState({
            [name]: !currentState
        });
    }

    onCheckboxDdInputChange = (checkBoxDataArrayName, checkBoxDataObjectIndex, e) => {

        const { name } = e.target;

        const currentState = this.state[checkBoxDataArrayName][checkBoxDataObjectIndex].isChecked;  // Get state(true/false) of Checkbox
        let checkBoxDataArray = this.state[checkBoxDataArrayName];  // Get complete Checkbox DropDown array

        if (name === "all") {
            const nextState = !currentState;

            for (var i = 0; i < checkBoxDataArray.length; i++) {
                checkBoxDataArray[i].isChecked = nextState;
            }

        } else {
            let checkBoxDataObjToBeUpdate = this.state[checkBoxDataArrayName][checkBoxDataObjectIndex];  // Get whole Checkbox Object using array index
            checkBoxDataObjToBeUpdate.isChecked = !currentState;  // Toggle Checkbox's state

            checkBoxDataArray[checkBoxDataObjectIndex] = checkBoxDataObjToBeUpdate;  // Update Checkbox Object in the Array
        }

        /* Get Selected Labels */
        let selectedLabels = [];
        for (let i = 0; i < checkBoxDataArray.length; i++) {
            if (checkBoxDataArray[i].isChecked === true) {
                selectedLabels.push(checkBoxDataArray[i].label);
            }

            if (checkBoxDataArray[i].name === 'all' && checkBoxDataArray[i].isChecked === true) {
                break;
            }
        }

        this.setState({
            [checkBoxDataArrayName]: checkBoxDataArray,
            ['selected_' + checkBoxDataArrayName]: selectedLabels.join(', ')
        });
    }

    changeCurrentSearchInReactTable = (searchString) => {
        this.setState({
            currentSearchInReactTable: searchString
        });
    }

    addOrRemovePost = (postId, addOrRemove) => {
        const allPosts = this.state.allPosts;

        let newAllPosts = [];
        for (let i = 0; i < allPosts.length; i++) {
            newAllPosts[i] = { ...allPosts[i] };

            if (newAllPosts[i].postId == postId) {
                newAllPosts[i].status = addOrRemove;
                newAllPosts[i].btnStatus = addOrRemove == 1 ? [true, false] : [false, true];
            }
        }

        this.setState({
            allPosts: newAllPosts,
            currentSearchInReactTable: '' // Have to do it yourself. Otherwise, search input in react-table remain same.
        });
        // (To be solved) After this, react-table will rerender and page number will switch back to 1. 
        // Try getting and setting current active page, nu. of rows in state just like current search

        axios.post('http://localhost:8000/api/addOrRemovePost', { 'postId': postId, 'status': addOrRemove }, {});
    }

    uploadPost = async (e) => {
        e.preventDefault();

        this.setState({
            error: {},
            uploadMessage: {}
        });

        let { title, description, images, category, departments, cities, shareable, customerSpeakPost, inquiryPost } = this.state;
        title = title.trim();
        customerSpeakPost = customerSpeakPost ? '1' : '0';
        inquiryPost = inquiryPost ? '1' : '0';

        if (title === "") {
            this.setState({
                error: {
                    title: "Title is Mandatory"
                }
            });

            return;
        }

        let selectedDepartments = [];
        for (let i = 0; i < departments.length; i++) {
            if (departments[i].name === 'all' && departments[i].isChecked === true) {
                selectedDepartments.push('all');
                break;
            }

            if (departments[i].isChecked === true) {
                selectedDepartments.push(departments[i].name);
            }
        }

        if (selectedDepartments.length === 0) {
            this.setState({
                error: {
                    departments: "Department is Mandatory"
                }
            });
            return;
        }

        selectedDepartments = selectedDepartments.toString();

        let selectedCities = [];
        for (let i = 0; i < cities.length; i++) {
            if (cities[i].name === 'all' && cities[i].isChecked === true) {
                selectedCities.push('all');
                break;
            }

            if (cities[i].isChecked === true) {
                selectedCities.push(cities[i].name);
            }
        }

        if (selectedCities.length === 0) {
            this.setState({
                error: {
                    cities: "City is Mandatory"
                }
            });
            return;
        }

        if (images.length > 3) {
            this.setState({
                error: {
                    images: "Max 3 files are allowed per post"
                }
            });
            return;
        }

        /* Code for Validatation of Max Image Size comes here */

        this.setState({
            uploadingPost: true
        });

        selectedCities = selectedCities.toString();

        var formData = new FormData();
        formData.append('title', title);
        formData.append('description', description);
        formData.append('category', category);
        formData.append('departments', selectedDepartments);
        formData.append('cities', selectedCities);
        formData.append('shareable', shareable);
        formData.append('customerSpeakPost', customerSpeakPost);
        formData.append('inquiryPost', inquiryPost);
        /* Can't use "for" loop here because of asynchnous nature of Javascript. It won't wait for compression code to complete and will proceed without it. Therefore, have used callback approch ahead.  */

        let response = await axios.post('http://localhost:8000/posts', formData, {});
        response = response.data;  // Axios Response is already an Object

        // response = JSON.parse(response); // NO NEED. Bcoz, json_encode() is already done in PHP.

        if (response.status === 'success') {
            this.uploadPostImages(response, images, 0);

        } else {
            this.setState({
                uploadingPost: false,
                error: {},
                uploadMessage: {
                    status: 0,
                    message: response.message
                }
            });
        }
    }

    uploadPostImages = async (response, images, currentIndex) => {

        if (currentIndex === images.length) {

            this.setState({
                uploadMessage: {
                    status: 1,
                    message: response.message
                },
                allPosts: [response.insertedRecord, ...this.state.allPosts]  // 'btnStatus', 'editBtn' are added in 'insertedRecord' at backend itself
            });

            this.clearFormInputs();

            return;
        }

        var formData = new FormData();
        formData.append('postId', response.insertedRecord.postId);

        var imageSize = images[currentIndex].size;

        if (imageSize <= 500000) {  // If Image is less than 500kb. No need to compress.
            formData.append('image', images[currentIndex]);

            let currentResponse = await axios.post('http://localhost:8000/uploadPostImages', formData, {});

            if (currentResponse.data) {
                currentIndex++;
                this.uploadPostImages(response, images, currentIndex);
            }

        } else {
            var maxWidth = 800;

            var thisOfFunction = this;

            new Compressor(images[currentIndex], {

                quality: 0.8,
                maxWidth: maxWidth,   // JPEG = 1296, PNG = 792  // "maxHeight" would be set automatically by the natural aspect ratio
                convertSize: 2000000,  // 2MB = PNG files over this value will be converted to JPEGs. To disable this, just set the value to Infinity
                success(compressedImage) {

                    formData.append('image', compressedImage);

                    // Had to use ".then()" here because, "await" won't work(may be because, it is inside "new Compressor()).
                    axios.post('http://localhost:8000/uploadPostImages', formData, {}).then(currentResponse => {

                        if (currentResponse.data) {
                            currentIndex++;
                            thisOfFunction.uploadPostImages(response, images, currentIndex);
                        }
                    });
                }
            });
        }
    }

    clearFormInputs = () => {
        let { departments } = this.state;
        for (let i = 0; i < departments.length; i++) {
            departments[i].isChecked = false;
        }

        let { cities } = this.state;
        for (let i = 0; i < cities.length; i++) {
            cities[i].isChecked = false;
        }

        this.setState({
            uploadingPost: false,
            title: '',
            description: '',
            departments: departments,
            selected_departments: '',
            cities: cities,
            selected_cities: '',
            images: '',
            category: '0',
            shareable: '1',
            inquiryPost: false,
            customerSpeakPost: true,
            currentSearchInReactTable: '',
            error: {}
        });

        document.getElementsByName("images")[0].value = null;  // File input cannot be controlled by React. Therefore, You have change it's value explicitly. >[0]< is necessary.
    }

    render() {
        const { uploadingPost, title, description, images, categories, category, departments, selected_departments, cities, selected_cities, shareable, inquiryPost, customerSpeakPost, error, uploadMessage, allPosts, allPostsColumns, currentSearchInReactTable } = this.state;

        return (
            <div>

                <h2>POSTS</h2>

                <div className='formDiv'>
                    <TextInput
                        label='Title* :'
                        name='title'
                        htmlFor='postTitle'
                        value={title}
                        placeholder="Post Title"
                        onChange={this.onInputChange}
                        error={error.title}
                    />
                    <TextAreaInput
                        label='Description :'
                        name='description'
                        value={description}
                        htmlFor='postDescription'
                        onChange={this.onInputChange}
                        error={error.description}
                    />
                    <MultipleFilesInput
                        label='Images :'
                        name='images'
                        htmlFor='postImages'
                        onChange={this.onFileInputChange}
                        error={error.images}
                    />
                    <SelectDdInput
                        name='category'
                        label='Category :'
                        htmlFor='postCategories'
                        onChange={this.onInputChange}
                        selectedOption={category}
                        options={categories}
                    />
                    <CheckboxDdInput
                        label='Departments* :'
                        htmlFor='postDepartments'
                        dropDownButtonName='Select Departments'
                        onChange={this.onCheckboxDdInputChange}
                        checkBoxDataArray={departments}
                        checkBoxDataArrayName='departments'
                        error={error.departments}
                        selected={selected_departments}
                    />
                    <CheckboxDdInput
                        label='Cities* :'
                        htmlFor='postCities'
                        dropDownButtonName='Select Cities'
                        onChange={this.onCheckboxDdInputChange}
                        checkBoxDataArray={cities}
                        checkBoxDataArrayName='cities'
                        error={error.cities}
                        selected={selected_cities}
                    />
                    <RadioGroupInput
                        name='shareable'
                        onChange={this.onInputChange}
                        checkedRadioBtn={shareable}
                        options={
                            [
                                { for: 'shareableRadioBtn', label: 'Shareable', value: '1' },
                                { for: 'unshareableRadioBtn', label: 'UnShareable', value: '0' }
                            ]
                        }
                    />
                    <div className='form-group row' style={{ 'paddingLeft': '15px' }}> {/* 'form-group row' is written here for Checkbox Input because of the requirement to have multiple Checkbox in Single Row */}
                        <CheckBoxInput
                            name='inquiryPost'
                            label='Inquiry Post'
                            htmlFor='inquiryPostCheckbox'
                            onChange={this.onCheckBoxInputChange}
                            isChecked={inquiryPost}
                        />
                        <CheckBoxInput
                            name='customerSpeakPost'
                            label='Customer Speak Post'
                            htmlFor='customerSpeakPostCheckbox'
                            onChange={this.onCheckBoxInputChange}
                            isChecked={customerSpeakPost}
                        />
                    </div>

                    <div className={classnames('row col-md-12', {
                        'messageSuccess': uploadMessage.status === 1,
                        'messageFail': uploadMessage.status === 0
                    })}>
                        {uploadMessage.message}
                    </div>
                    <div className='row'>
                        {uploadingPost && <img src={uploading} alt='loader' className='uploadingLoader' />}
                    </div>

                    <div className='row'>
                        <button type="button" className="btn btn-primary" onClick={this.uploadPost} disabled={uploadingPost}>UPLOAD POST</button>
                        <PostPreview
                            btnColor='success'
                            btnClass='previewPostBtn'
                            disabled={uploadingPost}
                            postDetails={{ title: title, description: description, images: images }}
                        />
                    </div>
                </div>

                <hr />

                <Table
                    data={allPosts}
                    columns={allPostsColumns}
                    initialSort={{ id: 'postId', desc: true }}
                    currentSearch={currentSearchInReactTable}
                    handleSearchString={this.changeCurrentSearchInReactTable}
                />

                <div className="spaceDiv-md"></div>

            </div >
        );
    }
}

const AddOrRemovePostBtn = ({ postId, addOrRemove, handleOnClick, disabled }) => {
    return (
        <button
            type='button'
            className={classnames('btn', {
                'btn-success': addOrRemove === '1',
                'btn-danger': addOrRemove === '0'
            })}
            onClick={handleOnClick.bind(this, postId, addOrRemove)}
            disabled={disabled}
        >
            {addOrRemove === '1' ? 'Add' : 'Remove'}
        </button>
    );
}

/* Code for >AddOrRemovePostBtn.propTypes< comes here */

const PostPreview = ({ btnColor, btnClass, disabled, postDetails }) => {
    const [modal, setModal] = useState(false); // part of reactstrap
    const toggle = () => setModal(!modal);    // part of reactstrap

    let { title, description, images } = postDetails;

    //description = description.replace(/(?:\r\n|\r|\n)/g, "<br/>"); // "<br/>" Won't work here for some reason. So, Use CSS "white-space: pre-wrap;".

    var imageUrl = '';
    if (images) {
        imageUrl = window.URL.createObjectURL(images[0]);
    }

    return (
        <div>
            <Button color={btnColor} onClick={toggle} className={btnClass} disabled={disabled}>PREVIEW POST</Button>

            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>POST PREVIEW</ModalHeader>
                <ModalBody className='divWithLineBreaks'>
                    {
                        imageUrl && <div> <img src={imageUrl} className='previewImage' alt='postPreviewImage' /> <br /> </div>
                    }
                    Title : {title} <br />
                    Description : {description}
                </ModalBody>
                <ModalFooter>
                    <Button color={btnColor} onClick={toggle}>OK</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

/* Code for >PostPreview.propTypes< comes here */

const PostEditFuncComponent = ({ btnColor, btnClass, disabled, postId, updateAllPosts, togglePageLoaderStatus }) => {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    // 'useState' hook with multiple values
    let [postDetails, setPostDetails] = useState({
        postImages: [],
        title: '',
        description: '',
        categories: [
            { label: 'Select Category', value: '0' }
        ],
        category: '0',
        departments: [
            { name: 'all', label: 'All', htmlFor: 'allPostDepartmentsEdit', isChecked: false }
        ],
        cities: [
            { name: 'all', label: 'All', htmlFor: 'allPostCitiesEdit', isChecked: false }
        ],
        images: '',
        shareable: '1',
        inquiryPost: false,
        customerSpeakPost: false,
        error: {}
    });

    const editPost = async () => {
        togglePageLoaderStatus();

        let response = await axios.get('http://localhost:8000/posts/' + postId + '/edit');
        response = response.data; // >response.postDetails< contains the main post details


        let newPostDetails = { ...postDetails };
        newPostDetails.postImages = response.postDetails.postImages;
        newPostDetails.title = response.postDetails.title;
        newPostDetails.description = response.postDetails.description;
        newPostDetails.category = String(response.postDetails.categoryId);
        let selectedDepartmentsArr = response.postDetails.departments.split(',');
        let selectedCitiesArr = response.postDetails.cities.split(',');
        newPostDetails.shareable = String(response.postDetails.shareable);
        newPostDetails.inquiryPost = (response.postDetails.inquiryPost === 1) ? true : false;
        newPostDetails.customerSpeakPost = (response.postDetails.customerSpeakPost === 1) ? true : false;


        let allCategories = response.categories;

        allCategories = allCategories.map((category, index) => {
            return { label: category.category, value: category.categoryId }
        });

        // Not concatenating to current 'categories' present in state, because from second iteration of modal there will be data from past execution too.
        newPostDetails.categories = [{ label: 'Select Category', value: '0' }].concat(allCategories);


        let allDepartments = response.departments;

        let selected_departments = [];
        allDepartments = allDepartments.map((department, index) => {
            let departmentId = String(department.departmentId);

            if (selectedDepartmentsArr.indexOf(departmentId) > -1 || selectedDepartmentsArr.indexOf('all') > -1) {
                selected_departments.push(department.department);

                return { name: departmentId, label: department.department, htmlFor: 'postDepartmentEdit' + departmentId, isChecked: true };

            } else {
                return { name: departmentId, label: department.department, htmlFor: 'postDepartmentEdit' + departmentId, isChecked: false };
            }
        });

        if (selectedDepartmentsArr.indexOf('all') > -1) {
            selected_departments = ['All'];

            allDepartments = [{ name: 'all', label: 'All', htmlFor: 'allPostDepartmentsEdit', isChecked: true }].concat(allDepartments);

        } else {
            allDepartments = [{ name: 'all', label: 'All', htmlFor: 'allPostDepartmentsEdit', isChecked: false }].concat(allDepartments);
        }

        newPostDetails.selected_departments = selected_departments.join(', ');
        newPostDetails.departments = allDepartments;


        let allCities = response.cities;

        let selected_cities = [];
        allCities = allCities.map((city, index) => {
            let cityId = String(city.cityId);

            if (selectedCitiesArr.indexOf(cityId) > -1 || selectedCitiesArr.indexOf('all') > -1) {
                selected_cities.push(city.city);

                return { name: cityId, label: city.city, htmlFor: 'postCityEdit' + cityId, isChecked: true };

            } else {
                return { name: cityId, label: city.city, htmlFor: 'postCityEdit' + cityId, isChecked: false };
            }
        });

        if (selectedCitiesArr.indexOf('all') > -1) {
            selected_cities = ['All'];

            allCities = [{ name: 'all', label: 'All', htmlFor: 'allPostCitiesEdit', isChecked: true }].concat(allCities);

        } else {
            allCities = [{ name: 'all', label: 'All', htmlFor: 'allPostCitiesEdit', isChecked: false }].concat(allCities);
        }

        newPostDetails.selected_cities = selected_cities.join(', ');
        newPostDetails.cities = allCities;


        setPostDetails(newPostDetails);

        toggle();  // OR >setModal(!modal)< here itself.
        togglePageLoaderStatus();
    }

    const handleChange = (inputType, e) => {
        const { name, value } = e.target;

        if (inputType === 'text' || inputType === 'radio' || inputType === 'select') {
            setPostDetails({ ...postDetails, [name]: value });

        } else if (inputType === 'file') {
            setPostDetails({ ...postDetails, [name]: e.target.files });

        } else if (inputType === 'checkbox') {
            let currentState = postDetails[name];

            setPostDetails({ ...postDetails, [name]: !currentState });
        }
    }

    const onCheckboxDdInputChange = (checkBoxDataArrayName, checkBoxDataObjectIndex, e) => {

        const { name } = e.target;

        // Instead of 'this.state', we will be using 'postDetails', which is name we have given to state of current functional component.
        const currentState = postDetails[checkBoxDataArrayName][checkBoxDataObjectIndex].isChecked;
        let checkBoxDataArray = postDetails[checkBoxDataArrayName];

        if (name === "all") {
            const nextState = !currentState;

            for (var i = 0; i < checkBoxDataArray.length; i++) {
                checkBoxDataArray[i].isChecked = nextState;
            }

        } else {
            let checkBoxDataObjToBeUpdate = postDetails[checkBoxDataArrayName][checkBoxDataObjectIndex];
            checkBoxDataObjToBeUpdate.isChecked = !currentState;

            checkBoxDataArray[checkBoxDataObjectIndex] = checkBoxDataObjToBeUpdate;
        }

        let selectedLabels = [];
        for (let i = 0; i < checkBoxDataArray.length; i++) {
            if (checkBoxDataArray[i].isChecked === true) {
                selectedLabels.push(checkBoxDataArray[i].label);
            }

            if (checkBoxDataArray[i].name === 'all' && checkBoxDataArray[i].isChecked === true) {
                break;
            }
        }

        setPostDetails({ ...postDetails, [checkBoxDataArrayName]: checkBoxDataArray, ['selected_' + checkBoxDataArrayName]: selectedLabels.join(', ') });
    }

    const editDone = async (e) => {
        e.preventDefault();

        let { title, description, images, category, departments, cities, shareable, customerSpeakPost, inquiryPost } = postDetails;
        title = title.trim();
        customerSpeakPost = customerSpeakPost ? '1' : '0';
        inquiryPost = inquiryPost ? '1' : '0';

        if (title === "") {
            setPostDetails({ ...postDetails, error: { title: "Title is Mandatory" } });

            return;
        }

        let selectedDepartments = [];
        for (let i = 0; i < departments.length; i++) {
            if (departments[i].name === 'all' && departments[i].isChecked === true) {
                selectedDepartments.push('all');
                break;
            }

            if (departments[i].isChecked === true) {
                selectedDepartments.push(departments[i].name);
            }
        }

        if (selectedDepartments.length === 0) {
            setPostDetails({ ...postDetails, error: { departments: "Department is Mandatory" } });

            return;
        }

        selectedDepartments = selectedDepartments.toString();

        let selectedCities = [];
        for (let i = 0; i < cities.length; i++) {
            if (cities[i].name === 'all' && cities[i].isChecked === true) {
                selectedCities.push('all');
                break;
            }

            if (cities[i].isChecked === true) {
                selectedCities.push(cities[i].name);
            }
        }

        if (selectedCities.length === 0) {
            setPostDetails({ ...postDetails, error: { cities: "City is Mandatory" } });

            return;
        }

        if (images.length > 3) {
            setPostDetails({ ...postDetails, error: { images: "Max 3 files are allowed per post" } });

            return;
        }

        /* Code for Validatation of Max Image Size comes here */

        togglePageLoaderStatus();

        selectedCities = selectedCities.toString();

        var formData = {};  // new FormData(); can't be use. Because '$request->input()' won't work in 'update()' of Laravel Resource Controller 
        formData.title = title;
        formData.description = description;
        formData.category = category;
        formData.departments = selectedDepartments;
        formData.cities = selectedCities;
        formData.shareable = shareable;
        formData.customerSpeakPost = customerSpeakPost;
        formData.inquiryPost = inquiryPost;

        let response = await axios.put('http://localhost:8000/posts/' + postId, formData, {});
        response = response.data;

        if (response.status === 'success') {
            editPostImages(response, images, 0);

        } else {
            togglePageLoaderStatus();
            alert(response.message);
        }
    }

    const editPostImages = async (response, images, currentIndex) => {

        if (currentIndex === images.length) {
            togglePageLoaderStatus();
            updateAllPosts();
            alert(response.message);
            toggle();

            return;
        }

        var formData = new FormData();
        formData.append('postId', postId);
        formData.append('currentIndex', currentIndex);

        var imageSize = images[currentIndex].size;

        if (imageSize <= 500000) {
            formData.append('image', images[currentIndex]);

            let currentResponse = await axios.post('http://localhost:8000/editPostImages', formData, {});

            if (currentResponse.data) {
                currentIndex++;
                editPostImages(response, images, currentIndex);
            }

        } else {
            var maxWidth = 800;

            new Compressor(images[currentIndex], {

                quality: 0.8,
                maxWidth: maxWidth,   // JPEG = 1296, PNG = 792  // "maxHeight" would be set automatically by the natural aspect ratio
                convertSize: 2000000,  // 2MB = PNG files over this value will be converted to JPEGs. To disable this, just set the value to Infinity
                success(compressedImage) {

                    formData.append('image', compressedImage);

                    // Had to use ".then()" here because, "await" won't work(may be because, it is inside "new Compressor()).
                    axios.post('http://localhost:8000/editPostImages', formData, {}).then(currentResponse => {

                        if (currentResponse.data) {
                            currentIndex++;
                            editPostImages(response, images, currentIndex);
                        }
                    });
                }
            });
        }
    }

    const { postImages, title, description, category, categories, departments, selected_departments, cities, selected_cities, shareable, inquiryPost, customerSpeakPost, error } = postDetails;

    return (
        <div>
            <Button color={btnColor} onClick={editPost} className={btnClass} disabled={disabled}>EDIT</Button>

            <Modal isOpen={modal} toggle={toggle} className='modal-xl'>
                <ModalHeader toggle={toggle}>EDIT POST</ModalHeader>
                <ModalBody className='divWithLineBreaks'>

                    <div className='formDiv'>

                        <div
                            className={(postImages.length > 0) ? 'row activeThing' : 'row inactiveThing'}
                            style={{ 'marginBottom': '15px' }}
                        >
                            <label className='col-md-2'>Existing Images :</label>
                            {
                                postImages.map((image, index) => {
                                    let imagePath = 'http://localhost:8000/postImages/' + image.name + '.' + image.extnsn;

                                    return <img src={imagePath} alt='post media' className="col-md-2" key={index} />;
                                })
                            }
                        </div>
                        <TextInput
                            label='Title* :'
                            name='title'
                            htmlFor='postTitleEdit'
                            value={title}
                            placeholder="Post Title"
                            onChange={handleChange.bind(this, 'text')}
                            error={error.title}
                        />
                        <TextAreaInput
                            label='Description :'
                            name='description'
                            value={description}
                            htmlFor='postDescriptionEdit'
                            onChange={handleChange.bind(this, 'text')}
                            error={error.description}
                        />
                        <MultipleFilesInput
                            label='Images :'
                            name='images'
                            htmlFor='postImagesEdit'
                            onChange={handleChange.bind(this, 'file')}
                            error={error.images}
                        />
                        <SelectDdInput
                            name='category'
                            label='Category :'
                            htmlFor='postCategoriesEdit'
                            onChange={handleChange.bind(this, 'select')}
                            selectedOption={category}
                            options={categories}
                        />
                        <CheckboxDdInput
                            label='Departments* :'
                            htmlFor='postDepartmentsEdit'
                            dropDownButtonName='Select Departments'
                            onChange={onCheckboxDdInputChange}
                            checkBoxDataArray={departments}
                            checkBoxDataArrayName='departments'
                            error={error.departments}
                            selected={selected_departments}
                        />
                        <CheckboxDdInput
                            label='Cities* :'
                            htmlFor='postCitiesEdit'
                            dropDownButtonName='Select Cities'
                            onChange={onCheckboxDdInputChange}
                            checkBoxDataArray={cities}
                            checkBoxDataArrayName='cities'
                            error={error.cities}
                            selected={selected_cities}
                        />
                        <RadioGroupInput
                            name='shareable'
                            onChange={handleChange.bind(this, 'radio')}
                            checkedRadioBtn={shareable}
                            options={
                                [
                                    { for: 'shareableRadioBtnEdit', label: 'Shareable', value: '1' },
                                    { for: 'unshareableRadioBtnEdit', label: 'UnShareable', value: '0' }
                                ]
                            }
                        />
                        <div className='form-group row' style={{ 'paddingLeft': '15px' }}>
                            <CheckBoxInput
                                name='inquiryPost'
                                label='Inquiry Post'
                                htmlFor='inquiryPostCheckboxEdit'
                                onChange={handleChange.bind(this, 'checkbox')}
                                isChecked={inquiryPost}
                            />
                            <CheckBoxInput
                                name='customerSpeakPost'
                                label='Customer Speak Post'
                                htmlFor='customerSpeakPostCheckboxEdit'
                                onChange={handleChange.bind(this, 'checkbox')}
                                isChecked={customerSpeakPost}
                            />
                        </div>

                    </div>

                </ModalBody>
                <ModalFooter>
                    <Button color={btnColor} onClick={editDone}>EDIT</Button>
                </ModalFooter>
            </Modal>
        </div >
    );
}

/* Code for >PostEdit.propTypes< comes here */

//  'export default' is necessary with 'conect()' because 'connect()' wraps your component in a new one, adding some useful features and returns a entirely new connected component. Therefore, you have to use following way.
const PostEdit = connect(null, { togglePageLoaderStatus })(PostEditFuncComponent);

export default Posts;