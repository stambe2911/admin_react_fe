
import React, { useState } from 'react';  // 'useState' is for 'CheckboxDdInput()'

import PropTypes from 'prop-types';
import classnames from 'classnames';

import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';  // for 'CheckboxDdInput()'

export const TextInput = (props) => {
    const { label, name, htmlFor, value, placeholder, onChange, error } = props;

    return (
        <div className='form-group'>
            <div className='row'>
                <label htmlFor={htmlFor} className='col-md-2'>{label}</label>
                <input
                    type='text'
                    name={name}
                    id={htmlFor}
                    value={value}
                    placeholder={placeholder}
                    autoComplete='off'
                    onChange={onChange}
                    className={classnames('form-control form-control-md col-md-8', {
                        'is-invalid': error
                    })}
                />
            </div>
            <div className='row'>
                <div className='offset-md-2 col-md-8 invalid-feedback' style={error && { display: 'block' }}>{error}</div>
            </div>
        </div>
    );
}

// Another way of extracting props
export const TextAreaInput = ({ name, label, value, htmlFor, onChange, error }) => {
    return (
        <div className='form-group'>
            <div className='row'>
                <label htmlFor={htmlFor} className='col-md-2'>{label}</label>
                <textarea
                    rows="5"
                    name={name}
                    id={htmlFor}
                    onChange={onChange}
                    value={value}
                    className={classnames('form-control col-md-8', {
                        'is-invalid': error
                    })}
                />
            </div>
            <div className='row'>
                <div className='offset-md-2 col-md-8 invalid-feedback' style={error && { display: 'block' }}>{error}</div>
            </div>
        </div>
    );
}

export const MultipleFilesInput = ({ label, name, htmlFor, onChange, error }) => {
    return (
        <div className='form-group'>
            <div className='row'>
                <label htmlFor={htmlFor} className='col-md-2'>{label}</label>
                <input
                    type='file'
                    name={name}
                    id={htmlFor}
                    onChange={onChange}
                    multiple
                    accept='.jpg,.jpeg,.png'
                    className={classnames('form-control form-control-md col-md-4', {
                        'is-invalid': error
                    })}
                />
            </div>
            <div className='row'>
                <div className='offset-md-2 col-md-8 invalid-feedback' style={error && { display: 'block' }}>{error}</div>
            </div>
        </div>
    );
}

export const RadioGroupInput = ({ name, onChange, checkedRadioBtn, options }) => {
    return (
        <div className="form-group custom-control custom-radio">
            <div className="row">
                {options.map((option, i) => {
                    return (
                        <div className="col-md-4" key={i}>
                            <input type="radio" className="custom-control-input" name={name} id={option.for} value={option.value} onChange={onChange} checked={checkedRadioBtn === option.value} />
                            <label className="custom-control-label" htmlFor={option.for}>{option.label}</label>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export const CheckBoxInput = ({ name, label, htmlFor, onChange, isChecked }) => {

    let attributes = {  // Other clean way of adding Attributes
        id: htmlFor,
        name: name,
        onChange: onChange,
        checked: isChecked
    };

    return (
        <div className="custom-control custom-checkbox mb-3 col-md-4">
            <input type="checkbox" className="custom-control-input" {...attributes} />
            <label className="custom-control-label" htmlFor={htmlFor}>{label}</label>
        </div>
    );
}

export const SelectDdInput = ({ name, label, htmlFor, onChange, selectedOption, options, error }) => {
    let attributes = {
        name: name,
        id: htmlFor,
        onChange: onChange,
        value: selectedOption
    };

    return (
        <div className='form-group'>
            <div className='row'>
                <label htmlFor={htmlFor} className='col-md-2'>{label}</label>
                <select
                    {...attributes}
                    className={classnames('custom-select custom-select-md mb-3 col-md-4', {
                        'is-invalid': error
                    })}
                >
                    {options.map((option, i) => {
                        return (
                            <option value={option.value} key={i}>{option.label}</option>
                        );
                    })}
                </select>
            </div>
            <div className='row'>
                <div className='offset-md-2 col-md-8 invalid-feedback' style={error && { display: 'block' }}>{error}</div>
            </div>
        </div>
    );
}

export const CheckboxDdInput = ({ label, htmlFor, dropDownButtonName, onChange, checkBoxDataArray, checkBoxDataArrayName, error, selected }) => {
    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);

    // Creating additional function for onChange because, have to pass parameter(ie. checkBoxDataIndex) from Child to Parent.
    const handleChange = (checkBoxDataArrayName, checkBoxDataObjectIndex, e) => {
        onChange(checkBoxDataArrayName, checkBoxDataObjectIndex, e);  // Calling Parent's onClick
    }

    return (
        <div className='form-group'>
            <div className='row'>
                <label htmlFor={htmlFor} className='col-md-2'>{label}</label>
                <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle>
                        {dropDownButtonName} <i className="fas fa-caret-down"></i>
                    </DropdownToggle>
                    <DropdownMenu>
                        {checkBoxDataArray.map((checkBoxDataObj, index) => {
                            return (
                                <DropdownItem toggle={false} key={index}>
                                    <div className="custom-control custom-checkbox mb-3 col-md-4">
                                        <input type="checkbox" className="custom-control-input" id={checkBoxDataObj.htmlFor} name={checkBoxDataObj.name} checked={checkBoxDataObj.isChecked} onChange={(e) => handleChange(checkBoxDataArrayName, index, e)} />
                                        <label className="custom-control-label" htmlFor={checkBoxDataObj.htmlFor}>{checkBoxDataObj.label}</label>
                                    </div>
                                </DropdownItem>
                            );
                        })}
                    </DropdownMenu>
                </ButtonDropdown>
                <div style={{ marginLeft: '10px' }}>
                    {selected}
                </div>
            </div>
            <div className='row'>
                <div className='offset-md-2 col-md-8 invalid-feedback' style={error && { display: 'block' }}>{error}</div>
            </div>
        </div>
    );
}


TextInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string
};

TextAreaInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string
};

MultipleFilesInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string
};

RadioGroupInput.propTypes = {
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    checkedRadioBtn: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired
};

CheckBoxInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    isChecked: PropTypes.bool.isRequired
};

SelectDdInput.propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    selectedOption: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    error: PropTypes.string
};

CheckboxDdInput.propTypes = {
    label: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    dropDownButtonName: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    checkBoxDataArray: PropTypes.array.isRequired,
    checkBoxDataArrayName: PropTypes.string.isRequired,
    error: PropTypes.string,
    selected: PropTypes.string

};

