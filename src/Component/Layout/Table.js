
import React from 'react';
import PropTypes from 'prop-types';

import { useTable, usePagination, useSortBy, useGlobalFilter, useAsyncDebounce } from 'react-table';
import styled from 'styled-components';

// 'styled' is styled-components's keyword
const Styles = styled.div`
    padding: 0.5rem;

    table {
        width: 100%;
        border: 2px solid #44BBFF;
        border-collapse: separate !important;
        border-spacing: 0;
        border-radius: 4px;

        thead{
            background-color: #E9F3FF;
        }

        tr {
            :last-child {
                td {
                    border-bottom: 0;
                }
            }
        }

        th,td {
            margin: 0;
            padding: 1rem;
            border-bottom: 2px solid #44BBFF;
            border-right: 2px solid #44BBFF;
            :last-child {
                border-right: 0;
            }
        }
    }

    .nuOfEntries{
        float: left;
    }

    .globalSearch{
        float: right;
    }

    select {
        padding: 0 6px;
    }

    .showingDetails{
        margin-top: 10px;
        float: left;
    }

    .pagination {
        margin-top: 10px;
        float: right;
    }

    .pagination > *{
        margin: 0 5px;
    }

    input[type=number]{
        width: 70px;
    }
`;

// (BUG) In OG code, If there is string still in GlobalFilter and you add new record, searched string is still present in text input.
// (BUG) 'added/removed' not working in search box.
const GlobalFilter = ({
    preGlobalFilteredRows,
    globalFilter,  // OG Code
    setGlobalFilter,
    handleSearchString,
    currentSearch
}) => {
    const count = preGlobalFilteredRows.length;
    //const [value, setValue] = React.useState(globalFilter);  // OG Code
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined);   // Set undefined to remove the filter entirely
    }, 200);

    return (
        <input
            type="text"
            placeholder={`Search in all ${count} records`}
            //value={value || ""}  // OG Code
            value={currentSearch || ""}
            onChange={e => {
                //setValue(e.target.value);  // OG Code
                onChange(e.target.value);
                handleSearchString(e.target.value);
            }}
            className="form-control form-control-sm"
        />
    );
}

export const Table = ({ columns, data, initialSort, currentSearch, handleSearchString }) => {
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,   // Instead of using 'rows', we'll use page, which has only the rows for the active page.
        rows,   // After "Global filter", we need count of filtered records to show as "Showing 11 to 18 of 18 Entries".
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        preGlobalFilteredRows,
        setGlobalFilter,
        state: { pageIndex, pageSize, globalFilter },
    } = useTable(
        {
            columns,   // columns: columns     // Providing Column Headers to react-table. 
            data,      // Providing Data to react-table. 
            initialState: {
                pageIndex: 0,
                pageSize: 5,  // (BUG) Work for the first time. But, after adding new record, Table doesn't refresh.
                sortBy: [initialSort],
                //globalFilter: currentSearch  // (BUG) Work for the first time. But, after adding new record, Table doesn't refresh.
            },
            disableSortRemove: true,  // Disable 'Default Sort' happening after the click on "Descending sorted" Column.
            autoResetGlobalFilter: true  // 'globalFilter' state will automatically reset if data is changed.
        },
        useGlobalFilter,
        useSortBy,
        usePagination
    );

    // (pageSize*pageIndex)+1 to (pageIndex+1)*pageSize
    const showingFrom = (pageSize * pageIndex) + 1;
    const showingTo = ((pageIndex + 1) * pageSize) > rows.length ? rows.length : ((pageIndex + 1) * pageSize);

    // Render Data Table UI
    return (
        <Styles>
            <div className="nuOfEntries">
                <select
                    value={pageSize}
                    onChange={e => {
                        setPageSize(Number(e.target.value))
                    }}
                >
                    {[5, 10, 20].map(pageSize => (
                        <option key={pageSize} value={pageSize}>
                            Show {pageSize}
                        </option>
                    ))}
                </select>
                <label style={{ 'marginLeft': '5px' }}> Entries </label>
            </div>
            <div className="globalSearch">
                <GlobalFilter
                    preGlobalFilteredRows={preGlobalFilteredRows}
                    globalFilter={globalFilter}
                    setGlobalFilter={setGlobalFilter}
                    handleSearchString={handleSearchString}
                    currentSearch={currentSearch}
                />
            </div>

            <table {...getTableProps()} style={{ 'marginTop': '5px' }}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                // Add the sorting props to control sorting. 
                                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                    {column.render('Header')}
                                    {/* Add a sort direction indicator */}
                                    <span style={{ 'float': 'right' }}>
                                        {column.isSorted
                                            ? column.isSortedDesc
                                                ? (<span>&#x25BC;</span>)
                                                : (<span>&#x25B2;</span>)
                                            : ''}
                                    </span>
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    )
                                })}
                            </tr>
                        )
                    }
                    )}
                </tbody>
            </table>

            <div className="showingDetails">
                Showing {showingFrom} to {showingTo} of {rows.length} Entries
            </div>

            <div className="pagination">
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>

                <button type="button" className="btn btn-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                    {'<<'}
                </button>{' '}
                <button type="button" className="btn btn-primary" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    {'<'}
                </button>{' '}
                <button type="button" className="btn btn-primary" onClick={() => nextPage()} disabled={!canNextPage}>
                    {'>'}
                </button>{' '}
                <button type="button" className="btn btn-primary" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                    {'>>'}
                </button>{' '}

                <span>
                    | Go to page {' '}
                    <input
                        type="number"
                        value={pageIndex + 1}
                        onChange={e => {
                            const page = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(page)
                        }}
                        style={{ width: '100px' }}
                    />
                </span>{' '}
            </div>
        </Styles>
    );
}

Table.propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    initialSort: PropTypes.object.isRequired,
    currentSearch: PropTypes.string.isRequired,
    handleSearchString: PropTypes.func.isRequired
};