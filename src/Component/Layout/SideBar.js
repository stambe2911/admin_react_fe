import React, { Component } from "react";

import { connect } from 'react-redux';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { switchPage } from '../../actions/layoutActions';
import { remove_admin } from '../../actions/adminActions';

class SideBar extends Component {

    switchPage = (pageLink, pageName) => {
        this.props.switchPage(pageLink, pageName);
    }

    logout = (e) => {
        e.preventDefault();

        this.props.remove_admin();

        this.props.history.push('/');
    }

    render() {

        const { sideDrawerStatus, activePageLink } = this.props.layoutStatus;

        return (
            <nav id="sidebar" className={sideDrawerStatus}>
                <div className="sidebar-header">
                    <h3 style={{ 'display': 'inline-block' }}>Lyncbiz</h3> <span style={{ 'float': 'right', 'cursor': 'pointer' }} onClick={this.logout}><i className="fas fa-power-off"></i></span>
                </div>

                <ul className="list-unstyled components">
                    <li className={`${activePageLink === 'statisticsLink' && 'active'}`} onClick={this.switchPage.bind(this, 'statisticsLink', 'statisticsPage')}>
                        <span className='sidebarMenu'>Statistics</span>
                    </li>
                    <li className={`${activePageLink === 'postsLink' && 'active'}`} onClick={this.switchPage.bind(this, 'postsLink', 'postsPage')}>
                        <span className='sidebarMenu'>Posts</span>
                    </li>
                    <li className={`${activePageLink === 'eventsLink' && 'active'}`} onClick={this.switchPage.bind(this, 'eventsLink', 'eventsPage')}>
                        <span className='sidebarMenu'>Events</span>
                    </li>
                    <li className={`${activePageLink === 'usersLink' && 'active'}`} onClick={this.switchPage.bind(this, 'usersLink', 'usersPage')}>
                        <span className='sidebarMenu'>Users</span>
                    </li>
                </ul>
            </nav>
        );
    }
}

SideBar.propTypes = {
    switchPage: PropTypes.func.isRequired,
    remove_admin: PropTypes.func.isRequired
};

const mapsStateToProps = (state) => ({
    layoutStatus: state.layoutR
});

export default compose(
    withRouter,
    connect(mapsStateToProps, { switchPage, remove_admin })
)(SideBar);