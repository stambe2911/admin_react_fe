import React, { Component } from "react";
import propTypes from 'prop-types';

import { connect } from 'react-redux';
import { changeSideDrawerStatus } from '../../actions/layoutActions';

import Statistics from '../pages/Statistics';
import Posts from '../pages/Posts';
import Events from '../pages/Events';

class PageContent extends Component {

    toggleSidebar = () => {
        this.props.changeSideDrawerStatus();
    }

    render() {
        const { activePageName } = this.props.layoutStatus;

        return (
            <div id="content" className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div>
                        <button type="button" id="sidebarCollapse" className="btn btn-info" onClick={this.toggleSidebar}>
                            <i className="fas fa-align-left"></i>
                        </button>
                    </div>
                </nav>

                <div id="statisticsPage" className={`container contentPage ${activePageName === 'statisticsPage' && 'active'}`}>
                    <Statistics />
                </div>

                <div id="postsPage" className={`container contentPage ${activePageName === 'postsPage' && 'active'}`}>
                    <Posts />
                </div>

                <div id="eventsPage" className={`container contentPage ${activePageName === 'eventsPage' && 'active'}`}>
                    <Events />
                </div>

                <div id="usersPage" className={`container contentPage ${activePageName === 'usersPage' && 'active'}`}>
                    <h2>USERS</h2>
                </div>
            </div>
        );
    }
}

PageContent.propTypes = {
    changeSideDrawerStatus: propTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    layoutStatus: state.layoutR
});

export default connect(mapStateToProps, { changeSideDrawerStatus })(PageContent);