
import { SET_ADMIN, REMOVE_ADMIN } from './types';

export const set_admin = (adminId, username) => {
    return {
        type: SET_ADMIN,
        adminId: adminId,
        username: username
    };
}

export const remove_admin = () => {
    return {
        type: REMOVE_ADMIN
    };
}