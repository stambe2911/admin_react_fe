
import { CHANGE_SIDE_DRAWER_STATUS, SWITCH_PAGE, TOGGLE_PAGE_LOADER_STATUS } from './types';

export const changeSideDrawerStatus = () => {
    return {
        type: CHANGE_SIDE_DRAWER_STATUS
    }
}

export const switchPage = (pageLink, pageName) => {
    return {
        type: SWITCH_PAGE,
        pageLink: pageLink,
        pageName: pageName
    }
}

export const togglePageLoaderStatus = () => {
    return {
        type: TOGGLE_PAGE_LOADER_STATUS
    }
}